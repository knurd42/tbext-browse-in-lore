browser.tabs.query({
  active: true,
  currentWindow: true,
}).then(tabs => {
  let tabId = tabs[0].id;
  browser.messageDisplay.getDisplayedMessage(tabId).then((message) => {
    browser.windows.openDefaultBrowser('http://lore.kernel.org/all/' + encodeURIComponent(message.headerMessageId))
    document.body.textContent = message.headerMessageId;
  });
  
  var timeout_span = 500;
  var timeout_var = setTimeout(() => {  window.close(); }, timeout_span);
  document.onmouseover = function() {
    clearTimeout(timeout_var);
  }
  document.onmouseout = function() {
    timeout_var = setTimeout(() => {  window.close(); }, timeout_span);
  }
});
