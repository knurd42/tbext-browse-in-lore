# Thunderbird extension "Browse in lore"

This extension adds a button to the message header toolbar that opens the
current message in the mailing list archives on lore.kernel.org using your
default browser; while doing so it shows the message ID briefly in a pop-up.

## Installation

1. Checkout this repo
2. Run `make` to generate the `xpi` file.
3. In Thunderbird, open the `Add-ons Manager`.
4. Click `Install Add-on From File...` and select the created `xpi` file.

## Known issues

* there are no checks if the message was sent to a mailing list archived on
  lore
* no way to configure the timeout of the pop-up 


