FILES=manifest.json archive-line.svg archive-line.license \
 popup.html popup.js LICENSE

XPI_NAME=browse-msg-in-lore@leemhuis.info.xpi

.PHONY: clean

all: $(XPI_NAME)

$(XPI_NAME): $(FILES)
	zip -r $@ $^

clean:
	rm *.xpi
